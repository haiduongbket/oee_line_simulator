from flask_restful import Resource, reqparse, request
from flask import Flask, request, render_template, redirect, url_for, session
from models import *
# from mqttclient import MqttClient

import os

missing_id_msg = {
    "message": {
        "id": "This field cannot be blank"
    }
}

# mqttClient = MqttClient()
# mqttClient.connect()

class GetLine(Resource):
    def get(self):
        msg = ''
        # global mqttClient
        objs = LineModel.findAll()
        for obj in objs:
            print(obj)
            # Create colunm ##############################################
        
        # mqttClient.publish("test", json.dumps(msg, ensure_ascii=False))
        # return redirect('/', messages=list(objs))
        return render_template('index.html', msg=msg)

class StartSshService(Resource):
    def get(self):
        # os.system("systemctl start run-broker.service")
        os.popen("sudo systemctl start run-broker.service", 'w').write('1')
        # os.system("sudo systemctl start sshd.service")
        return redirect('/')

class StopSshService(Resource):
    def get(self):
        # os.system("systemctl stop run-broker.service")
        os.popen("sudo systemctl stop run-broker.service", 'w').write('1')
        # os.system("sudo systemctl stop sshd.service")
        print("Stop SSH Service")

        return redirect('/')

class StatusSshService(Resource):
    def get(self):
        # os.system("systemctl status run-broker.service")
        os.popen("sudo systemctl status run-broker.service", 'w').write('1')
        # os.system("sudo systemctl status sshd.service")

        return redirect('/')
