from flask import Flask, request, render_template, redirect, url_for, session
from flaskext.mysql import MySQL

from flask_restful import Api
from flask_jwt_extended import JWTManager

import re
import jwt
from bson.json_util import dumps
import json

from mqttclient import MqttClient

app = Flask(__name__)
api = Api(app)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'rostek2020'
app.config['MYSQL_DATABASE_DB'] = 'rostekoee'
app.config['MYSQL_DATABASE_Host'] = 'localhost'
app.config['SECRET_KEY'] = '123456'

app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
# jwt = JWTManager(app)

# app.config['JWT_BLACKLIST_ENABLED'] = True
# app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

mysql.init_app(app)

conn = mysql.connect()
cursor =conn.cursor()

mqttClient = MqttClient()
mqttClient.connect()

import controllers
from models import LineModel

api.add_resource(controllers.GetLine, '/getLine')
api.add_resource(controllers.StartSshService, '/start_ssh_service')
api.add_resource(controllers.StopSshService, '/stop_ssh_service')
api.add_resource(controllers.StatusSshService, '/status_ssh_service')

@app.route('/')
def main():
    objs = LineModel.findAll()

    message = json.dumps(objs, ensure_ascii=False)
    # message = objs
    # print(message)
    return render_template('index.html', msg=message)
    # return render_template('index.html', msg=objs[0])

@app.route('/<path:dummy>')
def fallback(dummy):
    global mqttClient
    mqttClient.publish(dummy, "1")
    mqttClient.publish("/test", dummy)
    
    return redirect('/')
    
