from main import conn, cursor
from configure import *
from bson.json_util import dumps
import json

## Database model ##
class ObjectModel(dict):
    @classmethod
    def getDb(self):
        print("Please define database")
        return None

    def info(self):
        return self
        # return json.dumps(self, ensure_ascii=False)

    def save(self):
        obj = self.getDb().find({"id":self['id']},{"_id":0}).limit(1)
        if not obj.count():
            self.getDb().insert(self)
            return True
        else:
            return False

    def update(self):
        self.getDb().update({'id':self['id']},{ '$set': self})
    
    def delete(self):
        self.getDb().remove({'id':self['id']})

## Machine ##
class MachineModel(ObjectModel):
    @classmethod
    def getDb(cls):
        return db.machines

    @classmethod
    def findObject(cls, id):
        obj = MachineModel.getDb().find({"id":id},{"_id":0}).limit(1)
        if obj.count():
            listobj = list(obj)
            return MachineModel(listobj[0])
        else:
            return None

## Line ##
class LineModel(ObjectModel):
    @classmethod
    def getDb(cls):
        return db.lines

    @classmethod
    def findObject(cls, id):
        obj = LineModel.getDb().find({"id":id},{"_id":0}).limit(1)
        if obj.count():
            listobj = list(obj)
            return LineModel(listobj[0])
        else:
            return None

    def detail(self):
        lineDetail = self.copy()
        lineDetail['machine_ids'] = []

        for machineId in self['machine_ids']:
            machine = MachineModel.findObject(machineId)
            if machine:
                lineDetail['machine_ids'].append(machine)
        
        # return json.dumps(lineDetail, ensure_ascii=False)
        return lineDetail

    @classmethod
    def findAll(cls):
        objs = LineModel.getDb().find({},{"_id":0})
        # listobj = list(objs)
        # return list(objs)
        # print(objs)
        return list(objs)