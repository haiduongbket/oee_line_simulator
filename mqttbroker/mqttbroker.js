const aedes = require('aedes')()
const httpServer = require('http').createServer()
const ws = require('websocket-stream')
const port = 8888

const server = require('net').createServer(aedes.handle)
const mport = 1883

ws.createServer({ server: httpServer }, aedes.handle)

httpServer.listen(port, function () {
  console.log('websocket server listening on port ', port)
})

server.listen(mport, function () {
  console.log('server started and listening on port ', mport)
})
