import paho.mqtt.client as mqtt
import _thread
import time

##------------------------ MQTT CALL BACK EVENT
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+ str(rc))
    try:
        client.subscribe("/test")
    except Exception as e:
        print("Error: " + str(e))
        print("Please restart server.....")
 
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    # print("on message")
    topic = msg.topic
    payload = msg.payload.decode('utf-8')
    print("on message: " + payload)

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))
   
def Schedule(client):
    while 1:
        msg = "hello"
        client.publish("/test", msg)
        print("publish to mqtt msg: " + msg)
        time.sleep(1)

# ##------------------------ MAIN PROCESS
# # client = mqtt.Client()
# client = mqtt.Client(transport="websockets")
# client.on_connect = on_connect
# client.on_message = on_message
# client.on_subscribe = on_subscribe
# # client.connect("localhost",1883, 60)
# client.connect("localhost", 8888, 60)

# # _thread.start_new_thread( Schedule, (client,) )
# ## _thread.start();

# client.loop_forever()


class MqttClient:
    def __init__(self):
        self.client = mqtt.Client(transport="websockets")
        self.client.on_connect = on_connect
        self.client.on_message = on_message
        self.client.on_subscribe = on_subscribe     

    def connect(self):
        err_code = self.client.connect("localhost", 8888, 60)
        if not err_code:
             print("MQTT connected!!!")
        # client.loop_forever()
        # _thread.start_new_thread( Schedule, (client,) )

    def publish(self, topic, message):
        self.client.publish(topic, message)

